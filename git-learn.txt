﻿
2014-07-01
=================================================================
设置记住密码（默认15分钟）
git config --global credential.helper cache

如果想自己设置时间，可以这样做：
git config credential.helper 'cache --timeout=3600'
这样就设置一个小时之后失效

长期存储密码：
1.git config --global credential.helper store
ssh-keygen -t rsa -C "email@email.com"
id_rsa.pub 内容粘贴到git服务器上
ssh -T git@git.oschina.net

2.vim .git-credentials
https://{username}:{password}@git.oschina.net

增加远程地址的时候带上密码也是可以的。(推荐)
http://yourname:password@git.oschina.net/name/project.git

core.quotepath=false
core.pager=less -r
user.name=your name
user.email=your email
color.ui=true
color.diff=auto
gui.encoding=utf-8
i18n.commitencoding=utf-8
credential.helper=store
core.repositoryformatversion=0
core.filemode=false
core.logallrefupdates=true

还原单个文件
git reset a84c4a61eaab2399d3ec3799c9d5afd259ed1bbd WebRoot/WEB-INF/lib/
git checkout WebRoot/WEB-INF/lib/文件

=================================================================

2014-07-06
=================================================================
Failed to mount folders in Linux guest. This is usually because
the "vboxsf" file system is not available. Please verify that
the guest additions are properly installed in the guest and
can work properly. The command attempted was:

mount -t vboxsf -o uid=`id -u vagrant`,gid=`getent group vagrant | cut -d: -f3`
jdk /jdk
mount -t vboxsf -o uid=`id -u vagrant`,gid=`id -g vagrant` jdk /jdk

解决办法：
1.vagrant plugin install vagrant-vbguest
2.vagrant reload
=================================================================

2014-07-08 svn 迁移到 git 
=================================================================
git svn clone "svn://192.168.1.250/hisupplier-en/showroom" "en-showroom" -s --username wangxingming --no-metadata --authors-file=users.txt
git svn clone "svn://192.168.1.250/hisupplier-en/showroom" "en-showroom" -sr 18164:HEAD  --username wangxingming --no-metadata --authors-file=users.txt
git svn clone "svn://192.168.1.250/hisupplier-en/account" "en-account" -T trunk/v3.x -r 18005:HEAD  --username wangxingming --no-metadata --authors-file=users.txt
git svn clone "svn://192.168.1.250/hisupplier-en/account" "en-account" --branches branches -r 18311:HEAD  --username wangxingming --no-metadata --authors-file=users.txt
 
=================================================================